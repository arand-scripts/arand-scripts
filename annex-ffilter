#!/bin/bash
#
# This script is intended to run as a tree-filter for git filter-branch
# It removes a set of files based on file endings and paths from git and adds
# them to git-annex.
#
# Usage is something along the lines of:
# rm /tmp/annex-ffilter.log
# git filter-branch -f --tree-filter 'ANNEX_FFILTER_LOG=/tmp/annex-ffilter.log annex-ffilter' --tag-name-filter cat -- branchname

echo
echo "starting filter"
export TMPDIR="$(mktemp -d --tmpdir annex-ffilter_XXXXXX)"

# global file to store previously annexed sha1<->file<->annex triplets
# this should be set in the execution of the tree-filter
if test "$ANNEX_FFILTER_LOG"
then
	globalfile=$ANNEX_FFILTER_LOG
else
	echo "error: ANNEX_FFILTER_LOG is unset or empty"
	echo "annex-ffilter needs this variable for its global log, aborting"
	exit 1
fi

touch "$globalfile" || { echo "error: unable to access global file"; exit 1; }

# make a list of the files in the index that we want to have annexed
oldindexfiles=$(mktemp -u)
mkfifo "$oldindexfiles"
git ls-files -s \
	| grep -E '^[^	]*	.*\.(dds|dll|exe|icns|ico|iqm|jpg|lib|mpz|ogg|png|ttf|wpt|blend|tga|md3|it|wav|md5mesh|md5anim)$|^[^	]*	bin*/.*$|^[^	]*	src/xcode/Frameworks/.*$|^[^	]*	src/xcode/Frameworks/.*$|^[^	]*	redeclipse.app/.*$' \
	| sed 's/^[0-9]* \([a-z0-9]*\) [0-9]\t\(.*\)$/\1\t\2/' \
	| sort >"$oldindexfiles" &

# get files that have been annexed previously from the global log file
combinedfiles=$(mktemp)
cat "$oldindexfiles" "$globalfile" \
	| sort \
	| awk -F '\t' -v OFS='\t' '
		length($3) && !dups[$1]++ {
			sha256[$1] = $3
		}
		{
			sha1[$1]++
			names[$1,sha1[$1]] = $2
		}
		END {
			for (m in sha1) {
				for(n = 1; n <= sha1[m]; n++)
					print m, names[m,n], sha256[m]
			}
		}' \
	| sort >"$combinedfiles"

# above awk:
# if field3 exists and there are no duplicates of this line
# # then add to dups for this line and set the sha256 for this line to be field3
# for all
# # increase sha1 for this field1
# # and set name for [this field1 and count of sha1 for this field1] to field2
# # and set svnrev for [this field1 and count of sha1 for this field1] to field4
# at END
# # for each field1 index in sha1 and for the count of the same index in sha1
# # # print the field1 index, the name corresponding to the field1 and count index, the sha256 for this index

# get files that are already annexed (tab-separated field 3 exists)
oldannexfiles=$(mktemp -u)
mkfifo "$oldannexfiles"
grep -E '^[^	]+	[^	]+	[^	]+$' "$combinedfiles" \
	| comm -23 - <(sort "$globalfile") >"$oldannexfiles" &

echo "manually symlinking files which are already in the annex"
while IFS=$'\t' read -r _ file annexfile
do
	# remove quotes
	link=${file//\"}
	# hack to get relative link prefix
	old=$link
	link=${link#*/}
	prefix=
	while [[ "${prefix}${link}" != "${prefix}${old}" ]]
	do
		old=$link
		link=${link#*/}
		prefix=../${prefix}
	done
	link=${prefix}${annexfile}
	ln -sf "$(printf "$link")" "$(printf "$file")" &
done <"$oldannexfiles"

# copy of index before filtering
index=$(mktemp)
git ls-files -s >"$index"

# files in index which have not been annexed previously (does not have field 3)
indexfilter=$(mktemp -u)
mkfifo "$indexfilter"
sed -n 's/^\([^\t]*\)\t\([^\t]*\)\t$/000000 \1 0\t\2/p' "$combinedfiles" >"$indexfilter" &

# remove files we want to annex from index, awk is used to remove both dupes based on the filename
# (uniq/sort can't be limited to a field and print only unique files at the same time)
sort -m -t$'\t' -k2 <(sort -t$'\t' -k2 "$index") <(sort -t$'\t' -k2 "$indexfilter") \
	| awk -F'\t' '
		NR == 1 {
			x = $2
			l = $0
			next
		}
		x == $2 {
			d = 1
			next
		}
		x != $2 {
			if(!d)
				print l
			x = $2
			l = $0
			d = 0
		}
		END {
			if(!d) print l
		}' \
	| GIT_INDEX_FILE="$GIT_INDEX_FILE.new" git update-index --index-info

if [[ ! -s "$GIT_INDEX_FILE.new" ]]
then
	# create blank index
	GIT_INDEX_FILE="${GIT_INDEX_FILE}.new" git update-index --index-info <<EOF
100644 0000000000000000000000000000000000000000	annex-ffilter-dummy-file
0 0000000000000000000000000000000000000000	annex-ffilter-dummy-file
EOF
fi
mv "$GIT_INDEX_FILE.new" "$GIT_INDEX_FILE"

echo "annexing new files"
git annex add -c annex.alwayscommit=false . 1>/dev/null

# sha1<->file pairs which (should) have just been annexed
newindexfiles=$(mktemp -u)
mkfifo "$newindexfiles"
sed -n 's/^\([^\t]*\)\t\([^\t]*\)\t$/\1\t\2/p' "$combinedfiles" >"$newindexfiles" &

echo "reformatting symlinks created by git-annex"
while IFS=$'\t' read -r sha1 file
do
	link=$(readlink "$(printf "$file")")
	# parameter expansion to get rid of everything before match
	echo -e "${sha1}\t\
${file}\t\
${link/*\/.git\/annex\/objects\//.git/annex/objects/}" >&4
	ln -sf "${link#../../}" "$(printf "$file")" &
done <"$newindexfiles" 4>>"$globalfile"

git annex merge
rm -r "$TMPDIR"
wait
